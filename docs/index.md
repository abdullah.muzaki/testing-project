# Ini Adalah Contoh Tekdok

## Kenalan

Welcome to the Backstage TechDocs User Guide! This comprehensive guide will walk you through the features and functionality of Backstage's TechDocs plugin.

## Table of Contents

1. [Getting Started](#getting-started)
   - Installation
   - Configuration
   - Adding Documentation

2. [Document Structure](#document-structure)
   - Nested Sections
   - Images and Assets
   - Code Blocks

3. [Customization](#customization)
   - Theming
   - Styling

4. [Search](#search)
   - Configuring Search
   - Full-Text Search

5. [Versioning](#versioning)
   - Tagging Versions
   - Switching Between Versions

6. [Integrations](#integrations)
   - GitHub
   - CI/CD Integration

## Getting Started

### Installation

To install the TechDocs plugin, follow the installation instructions in the official Backstage documentation.

### Configuration

After installation, you need to configure the plugin in your Backstage app's `app-config.yaml` file. Refer to the configuration guide for more details.

### Adding Documentation

Once the plugin is set up, start adding your documentation in markdown format. Organize it into sections and pages using headings.

## Document Structure

### Nested Sections

You can create nested sections to organize your documentation better. Use sub-headings to represent subsections.

### Images and Assets

To include images or other assets, place them in the same repository or use public URLs.

### Code Blocks

Use triple backticks to add code blocks, and specify the programming language for syntax highlighting.

## Customization

### Theming

Customize the TechDocs UI theme to match your brand or preferences. Follow the theming guide for step-by-step instructions.

### Styling

If you want granular control, you can apply custom CSS styles to TechDocs. Refer to the styling guide for more information.

## Search

### Configuring Search

Configure TechDocs search to enable fast and accurate document search capabilities.

### Full-Text Search

TechDocs supports full-text search, allowing users to find content across all documents.

## Versioning

### Tagging Versions

Tag specific versions of your documentation to maintain historical records.

### Switching Between Versions

Easily switch between different versions of the documentation using the version selector.

## Integrations

### GitHub

TechDocs integrates seamlessly with GitHub, automatically pulling and updating documentation from your repositories.

### CI/CD Integration

Integrate TechDocs into your CI/CD pipeline to automate documentation updates and deployment.

For more in-depth explanations and advanced usage, refer to the complete TechDocs documentation available on the Backstage website.
[Open Google Slides Presentation](https://docs.google.com/presentation/d/e/2PACX-1vTd94H445stTNuxohmbybKmIpUdjwNlP8BF5wwZTs4_GaAjYfzL_Ze76Xdru4fNMO-WbOhWqr_7FSY7/pub?start=false&loop=false&delayms=3000)
<kbd>Click Me</kbd>